package com.itheima.ioc;

import com.itheima.ioc.dao.UserDao;
import com.itheima.ioc.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao = (UserDao) applicationContext.getBean("userDao");
        System.out.println(userDao);
        UserService userService = (UserService) applicationContext.getBean("userService");
        System.out.println(userService);
    }


}
